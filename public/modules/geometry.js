
import { vectorToCoords, coordsToVector, negate, intersectionPoint } from './utils.js';
import scene from './scene.js';

/**
 * Transforms virtual coordinates to screen coordinates.
 */
function screenCoords([x, y]) {
    const dim = window.state.canvasDimensions;
    let point = math.matrix([
        [x],
        [y],
        [1]]);
    const transformation = math.matrix([
        [1, 0, -dim.virtualTopLeft[0]],
        [0, -1, dim.virtualTopLeft[1]]]);
    point = math.multiply(transformation, point);
    const coords = vectorToCoords(point)
    return [Math.round(coords[0]), Math.round(coords[1])];
};

/**
 * Transforms screen coordinates to virtual coordinates.
 */
function virtualCoords([x, y]) {
    const dim = window.state.canvasDimensions;
    const s = dim.scaleFactor;
    let point = math.matrix([
        [x * s],
        [y * s],
        [1]]);
    const transformation = math.matrix([
        [1, 0, dim.virtualTopLeft[0]],
        [0, -1, dim.virtualTopLeft[1]]]);
    point = math.multiply(transformation, point);
    return vectorToCoords(point);
};

/**
 * Calculates the path of ray that is reflected in the box.
 */
const threshold = 0.01; // for floating number imprecision
function reflectedRay(p1, p2, ray) {
    ray.push(p2);
    // intersection with left mirror:
    const l = intersectionPoint(p1, p2, scene.box[0], scene.box[1]);
    // intersection with right mirror:
    const r = intersectionPoint(p1, p2, scene.box[2], scene.box[3]);
    if (l && math.distance(l, p2) > threshold) {
        return reflectedRay(reflect(p1, l, -1, 1), l, ray);
    } else if (r && math.distance(r, p2) > threshold) {
        return reflectedRay(reflect(p1, r, -1, 1), r, ray);
    } else {
        ray.push(p1);
        return ray;
    };
};

/**
 * Rotates a point about the angle of (v1-v2) to the x-axis. 
 */
function rotateAboutVector(p_, v1, v2) {
    let p = coordsToVector(p_);
    const cos = (v2[0] - v1[0]) / math.distance(v1, v2);
    const sin = (v2[1] - v1[1]) / math.distance(v1, v2);
    const transformationMatrix = math.matrix([
        [cos, sin],
        [sin, -cos]]);
    p = math.multiply(transformationMatrix, p);
    return vectorToCoords(p);
};

/**
 * Reflect a point about the origin, horizontally or vertically.
 */
function reflect(p_, origin_, reflectX, reflectY) {
    const p = coordsToVector(p_);
    const origin = coordsToVector(origin_);
    let pOrigin = math.add(p, negate(origin));
    const transformationMatrix = math.matrix([[reflectX, 0], [0, reflectY]]);
    pOrigin = math.multiply(transformationMatrix, pOrigin);
    return vectorToCoords(math.add(origin, pOrigin));
};

/**
 * calculates the appropriate gradient vector for 
 * the fade effect of the reflected passive ray.
 */
function getGradientVector(p1_, p2_) {
    const p1 = coordsToVector(p1_);
    const p2 = coordsToVector(p2_);
    let p1p2 = math.add(p1, negate(p2));
    p1p2 = vectorToCoords(p1p2);
    return rotateAboutOrigin(p1p2, 90);
};

function rotateAboutOrigin(p_, angle) {
    let p = coordsToVector(p_);
    const transformationMatrix = math.matrix([
        [Math.cos(angle), Math.sin(angle)],
        [-Math.sin(angle), Math.cos(angle)]]);
    p = math.multiply(transformationMatrix, p);
    return vectorToCoords(p);
};

/**
 * Caculates the height at which the arrowhead of the
 * active ray should be placed.
 */
function getArrowHeadDistance(v1, v2) {
    const intersection = intersectionPoint(v1, v2, ...scene.boxOpening)
    let distance = 0;
    if (intersection) {
        distance = math.distance(scene.eye, intersection);
    };
    return distance;

};

export {
    screenCoords, virtualCoords, reflectedRay,
    getGradientVector, rotateAboutVector, getArrowHeadDistance
};
/**
 * Returns intersection point of two line segments.
 * If two segments lie on the same line, undefined is returned.
 */
function intersectionPoint(p1_,p2_,p3_,p4_){
    const p1 = coordsToVector(p1_);
    const p2 = coordsToVector(p2_);
    const p3 = coordsToVector(p3_);
    const p4 = coordsToVector(p4_);
    
    const p2p1 = math.add(p2,negate(p1));
    const p4p3 = math.add(p4,negate(p3));
    const p3p1 = math.add(p3,negate(p1));
    const m = math.concat(p2p1,negate(p4p3));
    if (math.det(m) !== 0) {
        const inv = math.inv(m);
        const scalar = math.multiply(inv,p3p1).get([0,0]);
        const point = math.add(p1,math.dotMultiply(p2p1,scalar));
        const segment1Length = distance(p2,p1);
        const segment2Length = distance(p3,p4);
        if (distance(p1,point) <= segment1Length &&
            distance(p2,point) <= segment1Length && 
            distance(p3,point) <= segment2Length && 
            distance(p4,point) <= segment2Length 
        ) {
            return vectorToCoords(point);
        } else return undefined;
    } else return undefined;
};

/**
 * Checks if a point lies within a polygon.
 * Straightforward interpretation of crossing number algorithm.
 */
function pointInPolygon(point,polygon) {
    let topLeft = [...window.state.canvasDimensions.virtualTopLeft];
    topLeft[0] -= 50;
    topLeft[1] += 50;
    let numberOfIntersections = 0;
    for (let i = 0; i < polygon.length - 1; i++) {
        let intersection = intersectionPoint(topLeft,point, polygon[i],polygon[i + 1]);
        if (intersection) numberOfIntersections++;
    };
    return numberOfIntersections % 2 === 1;
};


function negate(p) {
    return math.dotMultiply(p,-1);
};

/**
 * a simple wrapper around the math.distance function that
 * to work with matrices instead of arrays
 */
function distance(p1,p2) {
    return math.distance(
        [p1.get([0,0]),p1.get([1,0])],
        [p2.get([0,0]),p2.get([1,0])]
    )
};

function coordsToVector([x,y]) {
    return math.matrix([[x],[y]])
};

function vectorToCoords(p) {
    return [p.get([0,0]),p.get([1,0])];
};

function createSVGElement(string) {
    return document.createElementNS('http://www.w3.org/2000/svg',string);
};



export {intersectionPoint, negate, createSVGElement,coordsToVector,vectorToCoords,pointInPolygon};
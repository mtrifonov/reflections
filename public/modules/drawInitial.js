import scene from './scene.js';
import { createSVGElement } from './utils.js';
import { screenCoords } from './geometry.js';

function drawInitialScene() {
    const canvas = window.state.mainCanvas;
    const dim = window.state.canvasDimensions;

    canvas.setAttribute("viewBox", `0 0  
    ${dim.virtualWidth} 
    ${dim.virtualHeight}`);

    canvas.appendChild(fillPolygon(scene.perceivedArea, "#e6e6e6"));
    canvas.appendChild(fillPolygon(scene.box, "#b3b3b3"));

    canvas.appendChild(createPassiveRay());
    canvas.appendChild(passiveRayGradient());
    canvas.appendChild(createPassiveRayEnd());

    canvas.appendChild(createControlRayMask());
    canvas.appendChild(createControlRay());

    canvas.appendChild(createActiveRay());
    canvas.appendChild(createActiveRayArrow());

    canvas.appendChild(drawLine([[-50, 98], [50, 98]], "#5e5e5e"));
    canvas.appendChild(drawLine(scene.mirrorLeft));
    canvas.appendChild(drawLine(scene.mirrorRight));

    for (let i = 0; i < scene.allGems.length; i++) {
        let gem = scene.allGems[i];
        canvas.appendChild(createImaginedGem([gem[0], gem[1]]));
    };
    canvas.appendChild(createRealGem());

    canvas.appendChild(createEye());

    canvas.appendChild(createPointer());
    canvas.appendChild(createPointerClip());
};

function createEye() {
    const el = createSVGElement("image");
    const width = 30;
    const height = width / 2;
    const coords = screenCoords([scene.eye[0] - width / 2, scene.eye[1] + height / 5]);
    el.setAttribute("x", coords[0]);
    el.setAttribute("y", coords[1]);
    el.setAttribute("width", width);
    el.id = "eye";
    return el;
};

function drawGem([x, y]) {
    const el = createSVGElement("image");
    const width = 15;
    const height = width * 0.8;
    const coords = screenCoords([x - width / 2, y + height / 2]);
    el.setAttribute("href", "SVG/gem.svg");
    el.setAttribute("x", coords[0]);
    el.setAttribute("y", coords[1]);
    el.setAttribute("width", width);
    return el;
};

function createImaginedGem([x, y]) {
    const el = drawGem([x, y]);
    el.setAttribute("clip-path", "url(#pointerClip)");
    return el;
}

function createRealGem() {
    const index = Math.floor(scene.allGems.length / 2);
    const realGem = scene.allGems[index];
    return drawGem(realGem);
};

function createPointer() {
    let el = createSVGElement("g");
    el.appendChild(createPointerShadow());
    el.appendChild(createPointerCircle());
    return el;
};

function createPointerShadow() {
    let el = createSVGElement("filter");
    el.id = "pointerShadow";
    let child = createSVGElement("feDropShadow");
    child.setAttribute("dx", "0");
    child.setAttribute("dy", "0");
    child.setAttribute("flood-color", "black");
    child.setAttribute("flood-opacity", "0.3");
    el.appendChild(child);
    return el;
};

function createPointerCircle() {
    let el = drawCircle(0, 0);
    el.id = "pointer";
    el.setAttribute("r", 25);
    el.setAttribute("fill-opacity", 0);
    el.setAttribute("stroke", "white");
    el.setAttribute("stroke-width", 3);
    el.setAttribute("filter", "url(#pointerShadow)");
    return el;
};

function createPointerClip() {
    let el = createSVGElement("clipPath");
    let circle = drawCircle(0, 0);
    circle.setAttribute("r", 25);
    circle.id = "pointerClipCircle";
    el.appendChild(circle);
    el.id = "pointerClip";
    return el;
};

function createControlRay() {
    let el = createSVGElement("path");
    el.setAttribute("fill-opacity", 0);
    el.setAttribute("stroke", "grey");
    el.setAttribute("stroke-width", 1);
    el.setAttribute("stroke-dasharray", "5,5");
    el.setAttribute("mask", "url(#controlRayMask)");
    el.id = "controlRay";
    return el;
};

function createControlRayMask() {
    const el = createSVGElement("mask");
    const viewport = fillPolygon(scene.viewport, "white");
    el.appendChild(viewport);
    const polygon = fillPolygon(scene.controlRayMask, "black");
    el.appendChild(polygon);
    el.id = "controlRayMask";
    return el;
};

function createActiveRay() {
    let el = createSVGElement("path");
    el.setAttribute("fill-opacity", 0);
    el.setAttribute("stroke", "#cf543e");
    el.setAttribute("stroke-width", 1.5);
    el.setAttribute("stroke-linejoin", "bevel");
    el.id = "activeRay";
    return el;
};

function createActiveRayArrow() {
    let el = createSVGElement("path");
    el.id = "activeRayArrow";
    el.setAttribute("fill", "#cf543e");
    return el;
};

function createPassiveRay() {
    let el = createSVGElement("path");
    el.setAttribute("fill-opacity", 0);
    el.setAttribute("stroke", "grey");
    el.setAttribute("stroke-width", 1.5);
    el.setAttribute("stroke-linejoin", "bevel");
    el.id = "passiveRay";
    return el;
};

function passiveRayGradient() {
    let el = createSVGElement("linearGradient");
    el.id = "rayGradient";
    let stop1 = createSVGElement("stop");
    stop1.id = "gStop1";
    stop1.setAttribute("offset", "0%");
    stop1.setAttribute("stop-color", "grey");
    let stop2 = createSVGElement("stop");
    stop2.id = "gStop2";
    stop2.setAttribute("offset", "100%");
    stop2.setAttribute("stop-color", "grey");
    el.appendChild(stop1);
    el.appendChild(stop2);
    el.setAttribute("x1", 0);
    el.setAttribute("y1", 0);
    return el;
}

function createPassiveRayEnd() {
    let el = createSVGElement("path");
    el.setAttribute("fill-opacity", 0);
    el.setAttribute("stroke", "url(#rayGradient)");
    el.setAttribute("stroke-width", 1.5);
    el.id = "passiveRayEnd";
    return el;
};

function drawCircle(x, y) {
    const el = createSVGElement("circle");
    el.setAttribute("cx", screenCoords([x, y])[0]);
    el.setAttribute("cy", screenCoords([x, y])[1]);
    el.setAttribute("r", 5);
    return el;
};

function fillPolygon(polygon, clr = "grey") {
    const el = createSVGElement("path");
    const firstVertex = screenCoords(polygon[0]);
    let d = `M ${firstVertex[0]} ${firstVertex[1]} `
    for (let i = 1; i < polygon.length; i++) {
        const currentVertex = screenCoords(polygon[i])
        d += `L ${currentVertex[0]} ${currentVertex[1]} `
    };
    d += "Z"
    el.setAttribute("d", d);
    el.setAttribute("fill", clr);
    return el;
};

function drawLine(line, clr = "#416fe6") {
    const el = createSVGElement("path");
    const firstVertex = screenCoords(line[0]);
    const secondVertex = screenCoords(line[1]);
    let d = "";
    d += `M ${firstVertex[0]} ${firstVertex[1]} `;
    d += `L ${secondVertex[0]} ${secondVertex[1]} `;
    el.setAttribute("d", d);
    el.setAttribute("stroke", clr);
    el.setAttribute("stroke-width", "3");
    return el;
};





export default drawInitialScene;
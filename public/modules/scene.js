
import { intersectionPoint } from './utils.js'

// CONFIG:
const eye = [0, -5];

const scene = {
    mirrorLeft: [
        [-50, 0],
        [-50, 100]
    ],
    mirrorRight: [
        [50, 0],
        [50, 100]
    ],
    box: [
        [-50, 0],
        [-50, 100],
        [50, 100],
        [50, 0],
        [-50, 0],
    ],
    controlRayMask: [
        [-50, -50],
        [-50, 100],
        [50, 100],
        [50, -50],
        [-50, -50],
    ],
    eye: eye,
    boxOpening: [
        [1000, 0],
        [-1000, 0]
    ],
    realGem: [0, 50],
    allGems: [
        [-300, 50],
        [-200, 50],
        [-100, 50],
        [0, 50],
        [100, 50],
        [200, 50],
        [300, 50],
    ],
    viewport: [
        // visible part of the scene 
        [-700, -20],
        [-700, 100],
        [700, 100],
        [700, -20],
        [-700, -20],

    ],
    perceivedArea: [
        [0, 0],
        intersectionPoint(
            eye,
            [-350, 50],
            [-1000, 0],
            [1000, 0],
        ),
        [-700, 100],
        [700, 100],
        intersectionPoint(
            eye,
            [350, 50],
            [-1000, 0],
            [1000, 0],
        ),
        [0, 0],
    ]
}

export default scene;



import drawInitialScene from './drawInitial.js';
import scene from './scene.js';
import { pointInPolygon, intersectionPoint } from './utils.js';
import { screenCoords, reflectedRay, getGradientVector, rotateAboutVector, getArrowHeadDistance } from './geometry.js';

/**
 * Renders the current frame.
 * If zoom has changed (or for the very first frame), 
 * the scene is built from scratch.
 */
function drawStep() {
    if (window.state.zoom !== window.state.lastKnownZoom) {
        window.state.lastKnownZoom = window.state.zoom;
        window.state.mainCanvas.innerHTML = "";
        window.state.init();
        drawInitialScene();
    };
    updatePointer();
    updateControlRay();
    updateActiveRay();
    updatePassiveRay();
    updateEyeColor();
};

function updateEyeColor() {
    const eye = document.getElementById("eye");
    const eyeHref = isMouseNearGem() ? "SVG/eye.svg" : "SVG/eyegrey.svg";
    eye.setAttribute("href", eyeHref);
};

function updatePointer() {
    const pointer = document.getElementById("pointer");
    const pointerClip = document.getElementById("pointerClipCircle");
    const coords = screenCoords(window.state.mouse);
    pointer.setAttribute("cx", coords[0]);
    pointer.setAttribute("cy", coords[1]);
    pointerClip.setAttribute("cx", coords[0]);
    pointerClip.setAttribute("cy", coords[1]);
    toggleVisibility(pointer,
        () => window.state.mouseOver
    );
};

function updateControlRay() {
    const controlRay = document.getElementById("controlRay");
    const coords = screenCoords(window.state.mouse);
    const firstVertex = screenCoords(window.state.mouse);
    const secondVertex = screenCoords(scene.eye);
    let d = "";
    d += `M ${firstVertex[0]} ${firstVertex[1]} `;
    d += `L ${secondVertex[0]} ${secondVertex[1]} `;
    controlRay.setAttribute("d", d);
    toggleVisibility(controlRay,
        () => pointInPolygon(window.state.mouse, scene.perceivedArea)
    );
};

function updateActiveRay() {
    const path = reflectedRay(window.state.mouse, scene.eye, []);
    updateActiveRayStart(path);
    updateActiveRayArrow(path);
};

function updateActiveRayStart(path) {
    const ray = document.getElementById('activeRay');
    let d = "";
    if (pointInPolygon(window.state.mouse, scene.perceivedArea)) {
        let firstVertex = getFirstVertex(path);
        d = `M ${firstVertex[0]} ${firstVertex[1]}`;
        for (let i = 1; i < path.length; i++) {
            const curVertex = screenCoords(path[i]);
            d += ` L ${curVertex[0]} ${curVertex[1]}`;
        };
    };
    ray.setAttribute("d", d);
    toggleVisibility(ray,
        () => pointInPolygon(window.state.mouse, scene.perceivedArea)
            && isMouseNearGem()
    );
};


function updateActiveRayArrow(path) {
    const arrowShape = calcActiveRayArrowPath(path);
    const rayArrow = document.getElementById('activeRayArrow');
    const firstVertex = screenCoords(arrowShape[0]);
    let d = `M ${firstVertex[0]} ${firstVertex[1]}`;
    for (let i = 1; i < arrowShape.length; i++) {
        let curVertex = screenCoords(arrowShape[i]);
        d += ` L ${curVertex[0]} ${curVertex[1]}`;
    };
    rayArrow.setAttribute("d", d);
    toggleVisibility(rayArrow,
        () => pointInPolygon(window.state.mouse, scene.perceivedArea)
            && isMouseNearGem()
    );
};


function calcActiveRayArrowPath(path) {
    const firstVertex = path[0];
    const secondVertex = path[1];
    const arrowShape = [[0, 0], [7, 4], [7, -4], [0, 0]];
    for (let i = 0; i < arrowShape.length; i++) {
        let curVertex = arrowShape[i];
        const distance = getArrowHeadDistance(firstVertex, secondVertex);
        curVertex = [curVertex[0] + (distance - 2), curVertex[1]]
        curVertex = rotateAboutVector(curVertex, firstVertex, secondVertex);
        curVertex = [curVertex[0], curVertex[1] + scene.eye[1]];
        arrowShape[i] = curVertex;
    };
    return arrowShape;
}

function updatePassiveRay() {
    const path = reflectedRay(window.state.mouse, scene.eye, []);
    updatePassiveRayStart(path);
    updatePassiveRayEnd(path);
    adjustGradient(path);
};

function updatePassiveRayStart(path) {
    const passiveRay = document.getElementById('passiveRay');
    let d = "";
    let firstVertex = getFirstVertex(path);
    d = `M ${firstVertex[0]} ${firstVertex[1]}`;
    for (let i = 1; i < path.length - 1; i++) {
        const curVertex = screenCoords(path[i]);
        d += ` L ${curVertex[0]} ${curVertex[1]}`;
    };
    passiveRay.setAttribute("d", d);
    toggleVisibility(passiveRay,
        () => pointInPolygon(window.state.mouse, scene.perceivedArea)
            && !isMouseNearGem()
    );

};

function updatePassiveRayEnd(path) {
    const rayEnd = document.getElementById('passiveRayEnd');
    let firstVertex = screenCoords(path[path.length - 2]);
    if (path.length - 2 === 0) {
        firstVertex = getFirstVertex(path);
    };
    const secondVertex = screenCoords(path[path.length - 1]);
    let d = `M ${firstVertex[0]} ${firstVertex[1]}
    L ${secondVertex[0]} ${secondVertex[1]}`
    rayEnd.setAttribute("d", d);
    toggleVisibility(rayEnd,
        () => pointInPolygon(window.state.mouse, scene.perceivedArea)
            && !isMouseNearGem()
    );
};

function adjustGradient(path) {
    const firstVertex = path[path.length - 2];
    const secondVertex = path[path.length - 1];
    adjustGradientVector(firstVertex, secondVertex);
    adjustStops(firstVertex, secondVertex);
};

function adjustGradientVector(v1, v2) {
    const gVector = getGradientVector(v1, v2);
    const gradient = document.getElementById("rayGradient");
    gradient.setAttribute("y1", Math.round(gVector[1]));
    gradient.setAttribute("y2", Math.round(gVector[1]));
};

function adjustStops(v1, v2) {
    const stop1 = document.getElementById('gStop1');
    const stop2 = document.getElementById('gStop2');
    if (v1[0] < v2[0]) {
        stop1.setAttribute("stop-opacity", "1");
        stop2.setAttribute("stop-opacity", "0");
    } else {
        stop1.setAttribute("stop-opacity", "0");
        stop2.setAttribute("stop-opacity", "1");
    };
};

// utils

function getFirstVertex(path) {
    let firstVertex = path[0];
    let intersection = intersectionPoint(path[0], path[1], ...scene.boxOpening);
    if (intersection) firstVertex = screenCoords(intersection);
    return firstVertex;
};

function toggleVisibility(el, condition) {
    if (condition()) el.setAttribute("visibility", "visible");
    else el.setAttribute("visibility", "hidden");
};

function isMouseNearGem() {
    const mouse = window.state.mouse;
    for (let i = 0; i < scene.allGems.length; i++) {
        if (math.distance(mouse, scene.allGems[i]) < 5) {
            return true;
        };
    };
    return false;
};

export default drawStep;
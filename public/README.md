# Light and Reflections Take-home

A simple interactive demonstrating the behavior of light rays in a box of mirrors.

The learner is invited to explore the perceived geometry of the box (including the reflections) by hunting for the mirror images of a gem placed within the actual geometry of the box.

Whenever such an image is found, the path taken by the light within the box is visualized.

## Libraries

This project uses the [math.js](https://mathjs.org) library to handle the linear algebra involved.  

## License
[MIT](https://choosealicense.com/licenses/mit/)

import drawStep from './modules/drawStep.js';
import scene from './modules/scene.js';
import { virtualCoords } from './modules/geometry.js';

window.state = {
    mainCanvas: document.getElementById('mainCanvas'),
    mouse: [0, 0],
    zoom: 5,
    lastKnownZoom: 0,
    mouseOver: true,
    init() {
        const obj = {};
        const r = mainCanvas.getBoundingClientRect();
        obj.screenWidth = r.width;
        obj.screenHeight = r.height;
        obj.virtualWidth = 1200 - (this.zoom * 70);
        obj.scaleFactor = obj.virtualWidth / obj.screenWidth;
        obj.virtualHeight = obj.screenHeight * obj.scaleFactor;
        obj.virtualTopLeft = [-obj.virtualWidth / 2, obj.virtualHeight / 2 + 40];
        this.canvasDimensions = obj;
    }
};

const mainCanvas = window.state.mainCanvas;
mainCanvas.addEventListener('mousemove', e => {
    window.state.mouse = virtualCoords([e.offsetX, e.offsetY]);
});
mainCanvas.addEventListener('mouseenter', e => {
    window.state.mouseOver = true;
});
mainCanvas.addEventListener('mouseleave', e => {
    window.state.mouseOver = false;
});

const zoomInput = document.getElementById("zoomInput");
zoomInput.addEventListener('change', e => {
    window.state.zoom = zoomInput.value;
});

function renderStep() {
    drawStep();
    window.requestAnimationFrame(renderStep);
};

renderStep();

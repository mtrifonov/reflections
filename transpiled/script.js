"use strict";

var _drawStep = _interopRequireDefault(require("./modules/drawStep.js"));

var _scene = _interopRequireDefault(require("./modules/scene.js"));

var _geometry = require("./modules/geometry.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

window.state = {
  mainCanvas: document.getElementById('mainCanvas'),
  mouse: [0, 0],
  zoom: 5,
  lastKnownZoom: 0,
  mouseOver: true,
  init: function init() {
    var obj = {};
    var r = mainCanvas.getBoundingClientRect();
    obj.screenWidth = r.width;
    obj.screenHeight = r.height;
    obj.virtualWidth = 1200 - this.zoom * 70;
    obj.scaleFactor = obj.virtualWidth / obj.screenWidth;
    obj.virtualHeight = obj.screenHeight * obj.scaleFactor;
    obj.virtualTopLeft = [-obj.virtualWidth / 2, obj.virtualHeight / 2 + 40];
    this.canvasDimensions = obj;
  }
};
var mainCanvas = window.state.mainCanvas;
mainCanvas.addEventListener('mousemove', function (e) {
  window.state.mouse = (0, _geometry.virtualCoords)([e.offsetX, e.offsetY]);
});
mainCanvas.addEventListener('mouseenter', function (e) {
  window.state.mouseOver = true;
});
mainCanvas.addEventListener('mouseleave', function (e) {
  window.state.mouseOver = false;
});
var zoomInput = document.getElementById("zoomInput");
zoomInput.addEventListener('change', function (e) {
  window.state.zoom = zoomInput.value;
});

function renderStep() {
  (0, _drawStep["default"])();
  window.requestAnimationFrame(renderStep);
}

;
renderStep();
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _utils = require("./utils.js");

// CONFIG:
var eye = [0, -5];
var scene = {
  mirrorLeft: [[-50, 0], [-50, 100]],
  mirrorRight: [[50, 0], [50, 100]],
  box: [[-50, 0], [-50, 100], [50, 100], [50, 0], [-50, 0]],
  controlRayMask: [[-50, -50], [-50, 100], [50, 100], [50, -50], [-50, -50]],
  eye: eye,
  boxOpening: [[1000, 0], [-1000, 0]],
  realGem: [0, 50],
  allGems: [[-300, 50], [-200, 50], [-100, 50], [0, 50], [100, 50], [200, 50], [300, 50]],
  viewport: [// visible part of the scene 
  [-700, -20], [-700, 100], [700, 100], [700, -20], [-700, -20]],
  perceivedArea: [[0, 0], (0, _utils.intersectionPoint)(eye, [-350, 50], [-1000, 0], [1000, 0]), [-700, 100], [700, 100], (0, _utils.intersectionPoint)(eye, [350, 50], [-1000, 0], [1000, 0]), [0, 0]]
};
var _default = scene;
exports["default"] = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _drawInitial = _interopRequireDefault(require("./drawInitial.js"));

var _scene = _interopRequireDefault(require("./scene.js"));

var _utils = require("./utils.js");

var _geometry = require("./geometry.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

/**
 * Renders the current frame.
 * If zoom has changed (or for the very first frame), 
 * the scene is built from scratch.
 */
function drawStep() {
  if (window.state.zoom !== window.state.lastKnownZoom) {
    window.state.lastKnownZoom = window.state.zoom;
    window.state.mainCanvas.innerHTML = "";
    window.state.init();
    (0, _drawInitial["default"])();
  }

  ;
  updatePointer();
  updateControlRay();
  updateActiveRay();
  updatePassiveRay();
  updateEyeColor();
}

;

function updateEyeColor() {
  var eye = document.getElementById("eye");
  var eyeHref = isMouseNearGem() ? "SVG/eye.svg" : "SVG/eyegrey.svg";
  eye.setAttribute("href", eyeHref);
}

;

function updatePointer() {
  var pointer = document.getElementById("pointer");
  var pointerClip = document.getElementById("pointerClipCircle");
  var coords = (0, _geometry.screenCoords)(window.state.mouse);
  pointer.setAttribute("cx", coords[0]);
  pointer.setAttribute("cy", coords[1]);
  pointerClip.setAttribute("cx", coords[0]);
  pointerClip.setAttribute("cy", coords[1]);
  toggleVisibility(pointer, function () {
    return window.state.mouseOver;
  });
}

;

function updateControlRay() {
  var controlRay = document.getElementById("controlRay");
  var coords = (0, _geometry.screenCoords)(window.state.mouse);
  var firstVertex = (0, _geometry.screenCoords)(window.state.mouse);
  var secondVertex = (0, _geometry.screenCoords)(_scene["default"].eye);
  var d = "";
  d += "M ".concat(firstVertex[0], " ").concat(firstVertex[1], " ");
  d += "L ".concat(secondVertex[0], " ").concat(secondVertex[1], " ");
  controlRay.setAttribute("d", d);
  toggleVisibility(controlRay, function () {
    return (0, _utils.pointInPolygon)(window.state.mouse, _scene["default"].perceivedArea);
  });
}

;

function updateActiveRay() {
  var path = (0, _geometry.reflectedRay)(window.state.mouse, _scene["default"].eye, []);
  updateActiveRayStart(path);
  updateActiveRayArrow(path);
}

;

function updateActiveRayStart(path) {
  var ray = document.getElementById('activeRay');
  var d = "";

  if ((0, _utils.pointInPolygon)(window.state.mouse, _scene["default"].perceivedArea)) {
    var firstVertex = getFirstVertex(path);
    d = "M ".concat(firstVertex[0], " ").concat(firstVertex[1]);

    for (var i = 1; i < path.length; i++) {
      var curVertex = (0, _geometry.screenCoords)(path[i]);
      d += " L ".concat(curVertex[0], " ").concat(curVertex[1]);
    }

    ;
  }

  ;
  ray.setAttribute("d", d);
  toggleVisibility(ray, function () {
    return (0, _utils.pointInPolygon)(window.state.mouse, _scene["default"].perceivedArea) && isMouseNearGem();
  });
}

;

function updateActiveRayArrow(path) {
  var arrowShape = calcActiveRayArrowPath(path);
  var rayArrow = document.getElementById('activeRayArrow');
  var firstVertex = (0, _geometry.screenCoords)(arrowShape[0]);
  var d = "M ".concat(firstVertex[0], " ").concat(firstVertex[1]);

  for (var i = 1; i < arrowShape.length; i++) {
    var curVertex = (0, _geometry.screenCoords)(arrowShape[i]);
    d += " L ".concat(curVertex[0], " ").concat(curVertex[1]);
  }

  ;
  rayArrow.setAttribute("d", d);
  toggleVisibility(rayArrow, function () {
    return (0, _utils.pointInPolygon)(window.state.mouse, _scene["default"].perceivedArea) && isMouseNearGem();
  });
}

;

function calcActiveRayArrowPath(path) {
  var firstVertex = path[0];
  var secondVertex = path[1];
  var arrowShape = [[0, 0], [7, 4], [7, -4], [0, 0]];

  for (var i = 0; i < arrowShape.length; i++) {
    var curVertex = arrowShape[i];
    var distance = (0, _geometry.getArrowHeadDistance)(firstVertex, secondVertex);
    curVertex = [curVertex[0] + (distance - 2), curVertex[1]];
    curVertex = (0, _geometry.rotateAboutVector)(curVertex, firstVertex, secondVertex);
    curVertex = [curVertex[0], curVertex[1] + _scene["default"].eye[1]];
    arrowShape[i] = curVertex;
  }

  ;
  return arrowShape;
}

function updatePassiveRay() {
  var path = (0, _geometry.reflectedRay)(window.state.mouse, _scene["default"].eye, []);
  updatePassiveRayStart(path);
  updatePassiveRayEnd(path);
  adjustGradient(path);
}

;

function updatePassiveRayStart(path) {
  var passiveRay = document.getElementById('passiveRay');
  var d = "";
  var firstVertex = getFirstVertex(path);
  d = "M ".concat(firstVertex[0], " ").concat(firstVertex[1]);

  for (var i = 1; i < path.length - 1; i++) {
    var curVertex = (0, _geometry.screenCoords)(path[i]);
    d += " L ".concat(curVertex[0], " ").concat(curVertex[1]);
  }

  ;
  passiveRay.setAttribute("d", d);
  toggleVisibility(passiveRay, function () {
    return (0, _utils.pointInPolygon)(window.state.mouse, _scene["default"].perceivedArea) && !isMouseNearGem();
  });
}

;

function updatePassiveRayEnd(path) {
  var rayEnd = document.getElementById('passiveRayEnd');
  var firstVertex = (0, _geometry.screenCoords)(path[path.length - 2]);

  if (path.length - 2 === 0) {
    firstVertex = getFirstVertex(path);
  }

  ;
  var secondVertex = (0, _geometry.screenCoords)(path[path.length - 1]);
  var d = "M ".concat(firstVertex[0], " ").concat(firstVertex[1], "\n    L ").concat(secondVertex[0], " ").concat(secondVertex[1]);
  rayEnd.setAttribute("d", d);
  toggleVisibility(rayEnd, function () {
    return (0, _utils.pointInPolygon)(window.state.mouse, _scene["default"].perceivedArea) && !isMouseNearGem();
  });
}

;

function adjustGradient(path) {
  var firstVertex = path[path.length - 2];
  var secondVertex = path[path.length - 1];
  adjustGradientVector(firstVertex, secondVertex);
  adjustStops(firstVertex, secondVertex);
}

;

function adjustGradientVector(v1, v2) {
  var gVector = (0, _geometry.getGradientVector)(v1, v2);
  var gradient = document.getElementById("rayGradient");
  gradient.setAttribute("y1", Math.round(gVector[1]));
  gradient.setAttribute("y2", Math.round(gVector[1]));
}

;

function adjustStops(v1, v2) {
  var stop1 = document.getElementById('gStop1');
  var stop2 = document.getElementById('gStop2');

  if (v1[0] < v2[0]) {
    stop1.setAttribute("stop-opacity", "1");
    stop2.setAttribute("stop-opacity", "0");
  } else {
    stop1.setAttribute("stop-opacity", "0");
    stop2.setAttribute("stop-opacity", "1");
  }

  ;
}

; // utils

function getFirstVertex(path) {
  var firstVertex = path[0];

  var intersection = _utils.intersectionPoint.apply(void 0, [path[0], path[1]].concat(_toConsumableArray(_scene["default"].boxOpening)));

  if (intersection) firstVertex = (0, _geometry.screenCoords)(intersection);
  return firstVertex;
}

;

function toggleVisibility(el, condition) {
  if (condition()) el.setAttribute("visibility", "visible");else el.setAttribute("visibility", "hidden");
}

;

function isMouseNearGem() {
  var mouse = window.state.mouse;

  for (var i = 0; i < _scene["default"].allGems.length; i++) {
    if (math.distance(mouse, _scene["default"].allGems[i]) < 5) {
      return true;
    }

    ;
  }

  ;
  return false;
}

;
var _default = drawStep;
exports["default"] = _default;
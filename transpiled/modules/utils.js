"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.coordsToVector = coordsToVector;
exports.createSVGElement = createSVGElement;
exports.intersectionPoint = intersectionPoint;
exports.negate = negate;
exports.pointInPolygon = pointInPolygon;
exports.vectorToCoords = vectorToCoords;

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

/**
 * Returns intersection point of two line segments.
 * If two segments lie on the same line, undefined is returned.
 */
function intersectionPoint(p1_, p2_, p3_, p4_) {
  var p1 = coordsToVector(p1_);
  var p2 = coordsToVector(p2_);
  var p3 = coordsToVector(p3_);
  var p4 = coordsToVector(p4_);
  var p2p1 = math.add(p2, negate(p1));
  var p4p3 = math.add(p4, negate(p3));
  var p3p1 = math.add(p3, negate(p1));
  var m = math.concat(p2p1, negate(p4p3));

  if (math.det(m) !== 0) {
    var inv = math.inv(m);
    var scalar = math.multiply(inv, p3p1).get([0, 0]);
    var point = math.add(p1, math.dotMultiply(p2p1, scalar));
    var segment1Length = distance(p2, p1);
    var segment2Length = distance(p3, p4);

    if (distance(p1, point) <= segment1Length && distance(p2, point) <= segment1Length && distance(p3, point) <= segment2Length && distance(p4, point) <= segment2Length) {
      return vectorToCoords(point);
    } else return undefined;
  } else return undefined;
}

;
/**
 * Checks if a point lies within a polygon.
 * Straightforward interpretation of crossing number algorithm.
 */

function pointInPolygon(point, polygon) {
  var topLeft = _toConsumableArray(window.state.canvasDimensions.virtualTopLeft);

  topLeft[0] -= 50;
  topLeft[1] += 50;
  var numberOfIntersections = 0;

  for (var i = 0; i < polygon.length - 1; i++) {
    var intersection = intersectionPoint(topLeft, point, polygon[i], polygon[i + 1]);
    if (intersection) numberOfIntersections++;
  }

  ;
  return numberOfIntersections % 2 === 1;
}

;

function negate(p) {
  return math.dotMultiply(p, -1);
}

;
/**
 * a simple wrapper around the math.distance function that
 * to work with matrices instead of arrays
 */

function distance(p1, p2) {
  return math.distance([p1.get([0, 0]), p1.get([1, 0])], [p2.get([0, 0]), p2.get([1, 0])]);
}

;

function coordsToVector(_ref) {
  var _ref2 = _slicedToArray(_ref, 2),
      x = _ref2[0],
      y = _ref2[1];

  return math.matrix([[x], [y]]);
}

;

function vectorToCoords(p) {
  return [p.get([0, 0]), p.get([1, 0])];
}

;

function createSVGElement(string) {
  return document.createElementNS('http://www.w3.org/2000/svg', string);
}

;
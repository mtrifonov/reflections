"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _scene = _interopRequireDefault(require("./scene.js"));

var _utils = require("./utils.js");

var _geometry = require("./geometry.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function drawInitialScene() {
  var canvas = window.state.mainCanvas;
  var dim = window.state.canvasDimensions;
  canvas.setAttribute("viewBox", "0 0  \n    ".concat(dim.virtualWidth, " \n    ").concat(dim.virtualHeight));
  canvas.appendChild(fillPolygon(_scene["default"].perceivedArea, "#e6e6e6"));
  canvas.appendChild(fillPolygon(_scene["default"].box, "#b3b3b3"));
  canvas.appendChild(createPassiveRay());
  canvas.appendChild(passiveRayGradient());
  canvas.appendChild(createPassiveRayEnd());
  canvas.appendChild(createControlRayMask());
  canvas.appendChild(createControlRay());
  canvas.appendChild(createActiveRay());
  canvas.appendChild(createActiveRayArrow());
  canvas.appendChild(drawLine([[-50, 98], [50, 98]], "#5e5e5e"));
  canvas.appendChild(drawLine(_scene["default"].mirrorLeft));
  canvas.appendChild(drawLine(_scene["default"].mirrorRight));

  for (var i = 0; i < _scene["default"].allGems.length; i++) {
    var gem = _scene["default"].allGems[i];
    canvas.appendChild(createImaginedGem([gem[0], gem[1]]));
  }

  ;
  canvas.appendChild(createRealGem());
  canvas.appendChild(createEye());
  canvas.appendChild(createPointer());
  canvas.appendChild(createPointerClip());
}

;

function createEye() {
  var el = (0, _utils.createSVGElement)("image");
  var width = 30;
  var height = width / 2;
  var coords = (0, _geometry.screenCoords)([_scene["default"].eye[0] - width / 2, _scene["default"].eye[1] + height / 5]);
  el.setAttribute("x", coords[0]);
  el.setAttribute("y", coords[1]);
  el.setAttribute("width", width);
  el.id = "eye";
  return el;
}

;

function drawGem(_ref) {
  var _ref2 = _slicedToArray(_ref, 2),
      x = _ref2[0],
      y = _ref2[1];

  var el = (0, _utils.createSVGElement)("image");
  var width = 15;
  var height = width * 0.8;
  var coords = (0, _geometry.screenCoords)([x - width / 2, y + height / 2]);
  el.setAttribute("href", "SVG/gem.svg");
  el.setAttribute("x", coords[0]);
  el.setAttribute("y", coords[1]);
  el.setAttribute("width", width);
  return el;
}

;

function createImaginedGem(_ref3) {
  var _ref4 = _slicedToArray(_ref3, 2),
      x = _ref4[0],
      y = _ref4[1];

  var el = drawGem([x, y]);
  el.setAttribute("clip-path", "url(#pointerClip)");
  return el;
}

function createRealGem() {
  var index = Math.floor(_scene["default"].allGems.length / 2);
  var realGem = _scene["default"].allGems[index];
  return drawGem(realGem);
}

;

function createPointer() {
  var el = (0, _utils.createSVGElement)("g");
  el.appendChild(createPointerShadow());
  el.appendChild(createPointerCircle());
  return el;
}

;

function createPointerShadow() {
  var el = (0, _utils.createSVGElement)("filter");
  el.id = "pointerShadow";
  var child = (0, _utils.createSVGElement)("feDropShadow");
  child.setAttribute("dx", "0");
  child.setAttribute("dy", "0");
  child.setAttribute("flood-color", "black");
  child.setAttribute("flood-opacity", "0.3");
  el.appendChild(child);
  return el;
}

;

function createPointerCircle() {
  var el = drawCircle(0, 0);
  el.id = "pointer";
  el.setAttribute("r", 25);
  el.setAttribute("fill-opacity", 0);
  el.setAttribute("stroke", "white");
  el.setAttribute("stroke-width", 3);
  el.setAttribute("filter", "url(#pointerShadow)");
  return el;
}

;

function createPointerClip() {
  var el = (0, _utils.createSVGElement)("clipPath");
  var circle = drawCircle(0, 0);
  circle.setAttribute("r", 25);
  circle.id = "pointerClipCircle";
  el.appendChild(circle);
  el.id = "pointerClip";
  return el;
}

;

function createControlRay() {
  var el = (0, _utils.createSVGElement)("path");
  el.setAttribute("fill-opacity", 0);
  el.setAttribute("stroke", "grey");
  el.setAttribute("stroke-width", 1);
  el.setAttribute("stroke-dasharray", "5,5");
  el.setAttribute("mask", "url(#controlRayMask)");
  el.id = "controlRay";
  return el;
}

;

function createControlRayMask() {
  var el = (0, _utils.createSVGElement)("mask");
  var viewport = fillPolygon(_scene["default"].viewport, "white");
  el.appendChild(viewport);
  var polygon = fillPolygon(_scene["default"].controlRayMask, "black");
  el.appendChild(polygon);
  el.id = "controlRayMask";
  return el;
}

;

function createActiveRay() {
  var el = (0, _utils.createSVGElement)("path");
  el.setAttribute("fill-opacity", 0);
  el.setAttribute("stroke", "#cf543e");
  el.setAttribute("stroke-width", 1.5);
  el.setAttribute("stroke-linejoin", "bevel");
  el.id = "activeRay";
  return el;
}

;

function createActiveRayArrow() {
  var el = (0, _utils.createSVGElement)("path");
  el.id = "activeRayArrow";
  el.setAttribute("fill", "#cf543e");
  return el;
}

;

function createPassiveRay() {
  var el = (0, _utils.createSVGElement)("path");
  el.setAttribute("fill-opacity", 0);
  el.setAttribute("stroke", "grey");
  el.setAttribute("stroke-width", 1.5);
  el.setAttribute("stroke-linejoin", "bevel");
  el.id = "passiveRay";
  return el;
}

;

function passiveRayGradient() {
  var el = (0, _utils.createSVGElement)("linearGradient");
  el.id = "rayGradient";
  var stop1 = (0, _utils.createSVGElement)("stop");
  stop1.id = "gStop1";
  stop1.setAttribute("offset", "0%");
  stop1.setAttribute("stop-color", "grey");
  var stop2 = (0, _utils.createSVGElement)("stop");
  stop2.id = "gStop2";
  stop2.setAttribute("offset", "100%");
  stop2.setAttribute("stop-color", "grey");
  el.appendChild(stop1);
  el.appendChild(stop2);
  el.setAttribute("x1", 0);
  el.setAttribute("y1", 0);
  return el;
}

function createPassiveRayEnd() {
  var el = (0, _utils.createSVGElement)("path");
  el.setAttribute("fill-opacity", 0);
  el.setAttribute("stroke", "url(#rayGradient)");
  el.setAttribute("stroke-width", 1.5);
  el.id = "passiveRayEnd";
  return el;
}

;

function drawCircle(x, y) {
  var el = (0, _utils.createSVGElement)("circle");
  el.setAttribute("cx", (0, _geometry.screenCoords)([x, y])[0]);
  el.setAttribute("cy", (0, _geometry.screenCoords)([x, y])[1]);
  el.setAttribute("r", 5);
  return el;
}

;

function fillPolygon(polygon) {
  var clr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "grey";
  var el = (0, _utils.createSVGElement)("path");
  var firstVertex = (0, _geometry.screenCoords)(polygon[0]);
  var d = "M ".concat(firstVertex[0], " ").concat(firstVertex[1], " ");

  for (var i = 1; i < polygon.length; i++) {
    var currentVertex = (0, _geometry.screenCoords)(polygon[i]);
    d += "L ".concat(currentVertex[0], " ").concat(currentVertex[1], " ");
  }

  ;
  d += "Z";
  el.setAttribute("d", d);
  el.setAttribute("fill", clr);
  return el;
}

;

function drawLine(line) {
  var clr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "#416fe6";
  var el = (0, _utils.createSVGElement)("path");
  var firstVertex = (0, _geometry.screenCoords)(line[0]);
  var secondVertex = (0, _geometry.screenCoords)(line[1]);
  var d = "";
  d += "M ".concat(firstVertex[0], " ").concat(firstVertex[1], " ");
  d += "L ".concat(secondVertex[0], " ").concat(secondVertex[1], " ");
  el.setAttribute("d", d);
  el.setAttribute("stroke", clr);
  el.setAttribute("stroke-width", "3");
  return el;
}

;
var _default = drawInitialScene;
exports["default"] = _default;
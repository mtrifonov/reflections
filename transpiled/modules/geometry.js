"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getArrowHeadDistance = getArrowHeadDistance;
exports.getGradientVector = getGradientVector;
exports.reflectedRay = reflectedRay;
exports.rotateAboutVector = rotateAboutVector;
exports.screenCoords = screenCoords;
exports.virtualCoords = virtualCoords;

var _utils = require("./utils.js");

var _scene = _interopRequireDefault(require("./scene.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
 * Transforms virtual coordinates to screen coordinates.
 */
function screenCoords(_ref) {
  var _ref2 = _slicedToArray(_ref, 2),
      x = _ref2[0],
      y = _ref2[1];

  var dim = window.state.canvasDimensions;
  var point = math.matrix([[x], [y], [1]]);
  var transformation = math.matrix([[1, 0, -dim.virtualTopLeft[0]], [0, -1, dim.virtualTopLeft[1]]]);
  point = math.multiply(transformation, point);
  var coords = (0, _utils.vectorToCoords)(point);
  return [Math.round(coords[0]), Math.round(coords[1])];
}

;
/**
 * Transforms screen coordinates to virtual coordinates.
 */

function virtualCoords(_ref3) {
  var _ref4 = _slicedToArray(_ref3, 2),
      x = _ref4[0],
      y = _ref4[1];

  var dim = window.state.canvasDimensions;
  var s = dim.scaleFactor;
  var point = math.matrix([[x * s], [y * s], [1]]);
  var transformation = math.matrix([[1, 0, dim.virtualTopLeft[0]], [0, -1, dim.virtualTopLeft[1]]]);
  point = math.multiply(transformation, point);
  return (0, _utils.vectorToCoords)(point);
}

;
/**
 * Calculates the path of ray that is reflected in the box.
 */

var threshold = 0.01; // for floating number imprecision

function reflectedRay(p1, p2, ray) {
  ray.push(p2); // intersection with left mirror:

  var l = (0, _utils.intersectionPoint)(p1, p2, _scene["default"].box[0], _scene["default"].box[1]); // intersection with right mirror:

  var r = (0, _utils.intersectionPoint)(p1, p2, _scene["default"].box[2], _scene["default"].box[3]);

  if (l && math.distance(l, p2) > threshold) {
    return reflectedRay(reflect(p1, l, -1, 1), l, ray);
  } else if (r && math.distance(r, p2) > threshold) {
    return reflectedRay(reflect(p1, r, -1, 1), r, ray);
  } else {
    ray.push(p1);
    return ray;
  }

  ;
}

;
/**
 * Rotates a point about the angle of (v1-v2) to the x-axis. 
 */

function rotateAboutVector(p_, v1, v2) {
  var p = (0, _utils.coordsToVector)(p_);
  var cos = (v2[0] - v1[0]) / math.distance(v1, v2);
  var sin = (v2[1] - v1[1]) / math.distance(v1, v2);
  var transformationMatrix = math.matrix([[cos, sin], [sin, -cos]]);
  p = math.multiply(transformationMatrix, p);
  return (0, _utils.vectorToCoords)(p);
}

;
/**
 * Reflect a point about the origin, horizontally or vertically.
 */

function reflect(p_, origin_, reflectX, reflectY) {
  var p = (0, _utils.coordsToVector)(p_);
  var origin = (0, _utils.coordsToVector)(origin_);
  var pOrigin = math.add(p, (0, _utils.negate)(origin));
  var transformationMatrix = math.matrix([[reflectX, 0], [0, reflectY]]);
  pOrigin = math.multiply(transformationMatrix, pOrigin);
  return (0, _utils.vectorToCoords)(math.add(origin, pOrigin));
}

;
/**
 * calculates the appropriate gradient vector for 
 * the fade effect of the reflected passive ray.
 */

function getGradientVector(p1_, p2_) {
  var p1 = (0, _utils.coordsToVector)(p1_);
  var p2 = (0, _utils.coordsToVector)(p2_);
  var p1p2 = math.add(p1, (0, _utils.negate)(p2));
  p1p2 = (0, _utils.vectorToCoords)(p1p2);
  return rotateAboutOrigin(p1p2, 90);
}

;

function rotateAboutOrigin(p_, angle) {
  var p = (0, _utils.coordsToVector)(p_);
  var transformationMatrix = math.matrix([[Math.cos(angle), Math.sin(angle)], [-Math.sin(angle), Math.cos(angle)]]);
  p = math.multiply(transformationMatrix, p);
  return (0, _utils.vectorToCoords)(p);
}

;
/**
 * Caculates the height at which the arrowhead of the
 * active ray should be placed.
 */

function getArrowHeadDistance(v1, v2) {
  var intersection = _utils.intersectionPoint.apply(void 0, [v1, v2].concat(_toConsumableArray(_scene["default"].boxOpening)));

  var distance = 0;

  if (intersection) {
    distance = math.distance(_scene["default"].eye, intersection);
  }

  ;
  return distance;
}

;